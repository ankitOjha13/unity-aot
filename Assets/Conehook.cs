﻿using System.Linq;
using Assets;
using UnityEngine;

public class Conehook : MonoBehaviour
{

    private GameObject camera;

    public bool isReleased;
    private GameObject player;
    private GameObject hook1;
    private GameObject hook2;
    private player playerObj;
    public float positionX;
    private FixedJoint joint;
    private Collider playerCollider;
    public bool isLatched;
    public bool isRetreating;
    private int touchStatus; //0-no touch,1-touching,2-touch released
    public bool isInCollision;
    private MeshCollider meshCollider;
    private Conehook hook1Obj;
    private Conehook hook2Obj;
    public Material red;
    public Material green;
    private MeshRenderer meshRenderer;

    private Rigidbody rb;
    // Use this for initialization

    public void ResetHook()
    {
        rb.isKinematic = true;
        meshCollider.isTrigger = true;
        transform.localPosition = new Vector3(positionX, -2f, 4);
        transform.localRotation = Quaternion.Euler(new Vector3(0, 1, 0));
        isReleased = false;
        isLatched = false;
        isRetreating = false;
        Destroy(joint);
    }

    void Start()
    {

        camera = GameObject.FindGameObjectsWithTag("MainCamera").Single();
        isReleased = false;
        var rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectsWithTag("Player").Single();
        hook1 = GameObject.FindGameObjectsWithTag("Hook").Single();
        hook2 = GameObject.FindGameObjectsWithTag("Hook2").Single();
        hook1Obj = hook1.GetComponent<Conehook>();
        hook2Obj = hook2.GetComponent<Conehook>();
        meshCollider = GetComponent<MeshCollider>();
        playerObj = player.GetComponent<player>();
        playerCollider = player.GetComponent<Collider>();
        joint = gameObject.GetComponent<FixedJoint>();
        meshRenderer = GetComponent<MeshRenderer>();
        isLatched = false;
        isRetreating = false;
        touchStatus = 0;
        isInCollision = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (hook1Obj.isInCollision || hook2Obj.isInCollision)
        {
            meshRenderer.material = red;
        }
        else
        {
            meshRenderer.material = green;
        }
        if (!isReleased)
        {
            //transform.position = camera.transform.position;
            //transform.rotation = camera.transform.rotation;

        }
        if (Input.touchCount == 0)
        {
            if (touchStatus == (int)TouchStatus.Touching)
            {
                touchStatus = (int)TouchStatus.Released;
            }
            else
            {
                touchStatus = (int)TouchStatus.NoTouch;
            }
        }
        if (touchStatus == (int)TouchStatus.Released || Input.GetMouseButtonDown(0))
        {
            if (isLatched || isReleased)
            {
                isRetreating = true;
                rb.isKinematic = true;
                meshCollider.isTrigger = true;
            }
            else if (playerObj.health > 0 && !hook1Obj.isInCollision && !hook2Obj.isInCollision)
            {
                isReleased = true;
                rb = GetComponent<Rigidbody>();
                rb.isKinematic = false;
                meshCollider.isTrigger = false;
                rb.AddForce(transform.forward * 320, ForceMode.Impulse);
            }
        }
        if (isRetreating)
        {
            //transform.LookAt(player.transform);
            //Vector3 direction = transform.position - new Vector3(player.transform.position.x + positionX, player.transform.position.y, player.transform.position.z);
            //rb.AddForce(transform.forward * 80, ForceMode.Force);
            var start = transform.position;
            playerObj.speed = 0;
            var target = new Vector3(player.transform.position.x + positionX, player.transform.position.y,
                player.transform.position.z);
            float speed = 320;
            transform.position = Vector3.MoveTowards(start, target, speed * Time.deltaTime);
            if (transform.position == target)
            {
                ResetHook();
            }

        }
    }


    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider != playerCollider)
        {
            isReleased = false;
            isLatched = true;
            rb.isKinematic = true;
            meshCollider.isTrigger = true;
            var midPoint = Vector3.Lerp(hook1.transform.position, hook2.transform.position, 0.5f);
            playerObj.isInMotion = true;
            playerObj.targetLocation = midPoint;
            playerObj.speed = 10;
        }
    }

    void OnTriggerStay(Collider collisionInfo)
    {
        isInCollision = true;

    }

    void OnTriggerExit(Collider collisionInfo)
    {
        isInCollision = false;
    }
}
